import { defineStore } from 'pinia'

export const useMenuStore = defineStore({
  id: 'menu',
  state: () => ({
    empresa: {},
    loading: false,
  }),
  actions: {
    async loadEmpresa(empresa) {
        this.$state.loading = true
          const responseArray = await fetch(`https://mimenu.duckdns.org/api/menu/${empresa}`)
        // const responseArray = await fetch(`http://localhost:8000/api/menu/${empresa}`)
      
        if (responseArray.status == 200){
            const responseJson = await responseArray.json()
            this.$state.loading = false
            this.$state.empresa = {
                ...responseJson,
            }
        }else{
            window.location.href = `/${empresa}/no-existe/`;
        }
    // else{
    //     this.$state.empresa = {}
    // }        
      
    },
  },
  getters: {
    getEmpresa(state) {
      return state.empresa
    },
    isLoading(state) {
      return state.loading
    },
  },
})
